﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication4
{
  public partial class _Default : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        ProductDropDown.DataSource = new[] {"Milk", "Cookies", "Vegemite"};
        ProductDropDown.DataBind();
      }

      MyButton.Text = "<script>alert('hello world!');</script>";
      MyLabel.Text = "<script>alert('hello world!');</script>";
      MyLabel.CssClass = "<script>alert('hello world!');</script>";
    }
  }
}