﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
  public partial class _Default : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      sessionStart.Text = Session["SessionStart"].ToString();

      Trace.Write("My trace message");
      Trace.Warn("My trace warn");

      var cookie = new HttpCookie("MyCookie", "Troy's cookie");
      cookie.HttpOnly = false;
      cookie.Secure = false;
      Response.Cookies.Add(cookie);

      if (!Page.IsPostBack)
      {
        ProductDropDown.DataSource = new object[] {"Milk", "Cookies", "Vegemite"};
        ProductDropDown.DataBind();
      }
    }
  }
}